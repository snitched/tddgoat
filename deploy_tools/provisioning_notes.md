Provisioning a new site
=====================

## Required packages:
* nginx
* Python 3.7
* virtualenv + pip
* Git

eg :
	sudo add-apt-repository ppa:repoz/ppa
	sudo apt update
	sudo apt install nginx git python3.7 python3.7-venv
	
## Nginx Virtual Host Config

* see nginx.template.conf
* replace DOMAIN with your domain name, e.g. staging.my-domain.com

## Systemd service

* see gunicorn-systemd.template.service
* replace DOMAIN with your domain name

## Folder structure:

Assume we have a user account at /home/username

/home/username
|__sites
   |-- Domain1
   |    |--.env
   |    |--db.sqlite3
   |    |--manage.py etc
   |    |--static
   |    |--virtualenv
   |-- Domain2
   |    |--.env
   |    |--db.sqlite3
   |    |--etc
